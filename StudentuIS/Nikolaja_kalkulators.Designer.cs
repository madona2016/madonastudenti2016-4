﻿namespace StudentuIS
{
    partial class Nikolaja_kalkulators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vienads = new System.Windows.Forms.Button();
            this.attirit = new System.Windows.Forms.Button();
            this.reizinat = new System.Windows.Forms.Button();
            this.dalit = new System.Windows.Forms.Button();
            this.pluss = new System.Windows.Forms.Button();
            this.minuss = new System.Windows.Forms.Button();
            this.nulle = new System.Windows.Forms.Button();
            this.devini = new System.Windows.Forms.Button();
            this.astoni = new System.Windows.Forms.Button();
            this.septini = new System.Windows.Forms.Button();
            this.sesi = new System.Windows.Forms.Button();
            this.pieci = new System.Windows.Forms.Button();
            this.cetri = new System.Windows.Forms.Button();
            this.tris = new System.Windows.Forms.Button();
            this.divi = new System.Windows.Forms.Button();
            this.viens = new System.Windows.Forms.Button();
            this.Ekrans = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // vienads
            // 
            this.vienads.Location = new System.Drawing.Point(266, 52);
            this.vienads.Name = "vienads";
            this.vienads.Size = new System.Drawing.Size(45, 87);
            this.vienads.TabIndex = 33;
            this.vienads.Text = "=";
            this.vienads.UseVisualStyleBackColor = true;
            this.vienads.Click += new System.EventHandler(this.button16_Click);
            // 
            // attirit
            // 
            this.attirit.Location = new System.Drawing.Point(217, 145);
            this.attirit.Name = "attirit";
            this.attirit.Size = new System.Drawing.Size(45, 34);
            this.attirit.TabIndex = 32;
            this.attirit.Text = "C";
            this.attirit.UseVisualStyleBackColor = true;
            this.attirit.Click += new System.EventHandler(this.attirit_Click);
            // 
            // reizinat
            // 
            this.reizinat.Location = new System.Drawing.Point(165, 145);
            this.reizinat.Name = "reizinat";
            this.reizinat.Size = new System.Drawing.Size(45, 34);
            this.reizinat.TabIndex = 31;
            this.reizinat.Text = "*";
            this.reizinat.UseVisualStyleBackColor = true;
            this.reizinat.Click += new System.EventHandler(this.pluss_Click);
            // 
            // dalit
            // 
            this.dalit.Location = new System.Drawing.Point(114, 145);
            this.dalit.Name = "dalit";
            this.dalit.Size = new System.Drawing.Size(45, 34);
            this.dalit.TabIndex = 30;
            this.dalit.Text = "/";
            this.dalit.UseVisualStyleBackColor = true;
            this.dalit.Click += new System.EventHandler(this.pluss_Click);
            // 
            // pluss
            // 
            this.pluss.Location = new System.Drawing.Point(63, 145);
            this.pluss.Name = "pluss";
            this.pluss.Size = new System.Drawing.Size(45, 34);
            this.pluss.TabIndex = 29;
            this.pluss.Text = "+";
            this.pluss.UseVisualStyleBackColor = true;
            this.pluss.Click += new System.EventHandler(this.pluss_Click);
            // 
            // minuss
            // 
            this.minuss.Location = new System.Drawing.Point(12, 145);
            this.minuss.Name = "minuss";
            this.minuss.Size = new System.Drawing.Size(45, 34);
            this.minuss.TabIndex = 28;
            this.minuss.Text = "-";
            this.minuss.UseVisualStyleBackColor = true;
            this.minuss.Click += new System.EventHandler(this.pluss_Click);
            // 
            // nulle
            // 
            this.nulle.Location = new System.Drawing.Point(12, 304);
            this.nulle.Name = "nulle";
            this.nulle.Size = new System.Drawing.Size(274, 23);
            this.nulle.TabIndex = 27;
            this.nulle.Text = "0";
            this.nulle.UseVisualStyleBackColor = true;
            this.nulle.Click += new System.EventHandler(this.nulle_Click);
            // 
            // devini
            // 
            this.devini.Location = new System.Drawing.Point(211, 194);
            this.devini.Name = "devini";
            this.devini.Size = new System.Drawing.Size(75, 23);
            this.devini.TabIndex = 26;
            this.devini.Text = "9";
            this.devini.UseVisualStyleBackColor = true;
            this.devini.Click += new System.EventHandler(this.nulle_Click);
            // 
            // astoni
            // 
            this.astoni.Location = new System.Drawing.Point(115, 194);
            this.astoni.Name = "astoni";
            this.astoni.Size = new System.Drawing.Size(75, 23);
            this.astoni.TabIndex = 25;
            this.astoni.Text = "8";
            this.astoni.UseVisualStyleBackColor = true;
            this.astoni.Click += new System.EventHandler(this.nulle_Click);
            // 
            // septini
            // 
            this.septini.Location = new System.Drawing.Point(12, 194);
            this.septini.Name = "septini";
            this.septini.Size = new System.Drawing.Size(75, 23);
            this.septini.TabIndex = 24;
            this.septini.Text = "7";
            this.septini.UseVisualStyleBackColor = true;
            this.septini.Click += new System.EventHandler(this.nulle_Click);
            // 
            // sesi
            // 
            this.sesi.Location = new System.Drawing.Point(211, 223);
            this.sesi.Name = "sesi";
            this.sesi.Size = new System.Drawing.Size(75, 23);
            this.sesi.TabIndex = 23;
            this.sesi.Text = "6";
            this.sesi.UseVisualStyleBackColor = true;
            this.sesi.Click += new System.EventHandler(this.nulle_Click);
            // 
            // pieci
            // 
            this.pieci.Location = new System.Drawing.Point(115, 223);
            this.pieci.Name = "pieci";
            this.pieci.Size = new System.Drawing.Size(75, 23);
            this.pieci.TabIndex = 22;
            this.pieci.Text = "5";
            this.pieci.UseVisualStyleBackColor = true;
            this.pieci.Click += new System.EventHandler(this.nulle_Click);
            // 
            // cetri
            // 
            this.cetri.Location = new System.Drawing.Point(12, 223);
            this.cetri.Name = "cetri";
            this.cetri.Size = new System.Drawing.Size(75, 23);
            this.cetri.TabIndex = 21;
            this.cetri.Text = "4";
            this.cetri.UseVisualStyleBackColor = true;
            this.cetri.Click += new System.EventHandler(this.nulle_Click);
            // 
            // tris
            // 
            this.tris.Location = new System.Drawing.Point(211, 252);
            this.tris.Name = "tris";
            this.tris.Size = new System.Drawing.Size(75, 31);
            this.tris.TabIndex = 20;
            this.tris.Text = "3";
            this.tris.UseVisualStyleBackColor = true;
            this.tris.Click += new System.EventHandler(this.nulle_Click);
            // 
            // divi
            // 
            this.divi.Location = new System.Drawing.Point(115, 254);
            this.divi.Name = "divi";
            this.divi.Size = new System.Drawing.Size(75, 29);
            this.divi.TabIndex = 19;
            this.divi.Text = "2";
            this.divi.UseVisualStyleBackColor = true;
            this.divi.Click += new System.EventHandler(this.nulle_Click);
            // 
            // viens
            // 
            this.viens.Location = new System.Drawing.Point(12, 254);
            this.viens.Name = "viens";
            this.viens.Size = new System.Drawing.Size(75, 29);
            this.viens.TabIndex = 18;
            this.viens.Text = "1";
            this.viens.UseVisualStyleBackColor = true;
            this.viens.Click += new System.EventHandler(this.nulle_Click);
            // 
            // Ekrans
            // 
            this.Ekrans.Location = new System.Drawing.Point(12, 12);
            this.Ekrans.Multiline = true;
            this.Ekrans.Name = "Ekrans";
            this.Ekrans.ReadOnly = true;
            this.Ekrans.Size = new System.Drawing.Size(221, 100);
            this.Ekrans.TabIndex = 17;
            this.Ekrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Nikolaja_kalkulators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 350);
            this.Controls.Add(this.vienads);
            this.Controls.Add(this.attirit);
            this.Controls.Add(this.reizinat);
            this.Controls.Add(this.dalit);
            this.Controls.Add(this.pluss);
            this.Controls.Add(this.minuss);
            this.Controls.Add(this.nulle);
            this.Controls.Add(this.devini);
            this.Controls.Add(this.astoni);
            this.Controls.Add(this.septini);
            this.Controls.Add(this.sesi);
            this.Controls.Add(this.pieci);
            this.Controls.Add(this.cetri);
            this.Controls.Add(this.tris);
            this.Controls.Add(this.divi);
            this.Controls.Add(this.viens);
            this.Controls.Add(this.Ekrans);
            this.Name = "Nikolaja_kalkulators";
            this.Text = "Nikolaja_kalkulators";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button vienads;
        private System.Windows.Forms.Button attirit;
        private System.Windows.Forms.Button reizinat;
        private System.Windows.Forms.Button dalit;
        private System.Windows.Forms.Button pluss;
        private System.Windows.Forms.Button minuss;
        private System.Windows.Forms.Button nulle;
        private System.Windows.Forms.Button devini;
        private System.Windows.Forms.Button astoni;
        private System.Windows.Forms.Button septini;
        private System.Windows.Forms.Button sesi;
        private System.Windows.Forms.Button pieci;
        private System.Windows.Forms.Button cetri;
        private System.Windows.Forms.Button tris;
        private System.Windows.Forms.Button divi;
        private System.Windows.Forms.Button viens;
        private System.Windows.Forms.TextBox Ekrans;
    }
}