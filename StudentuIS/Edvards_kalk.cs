﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace kalkulators
{
    public partial class Edvards_kalk : Form
    {
        double sk1, sk2, rez;
        string op = "";
        public Edvards_kalk()
        {
            InitializeComponent();
        }

        private void poga0_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Rezultats.Text = Rezultats.Text + btn.Text;

        }

        private void minuss_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            op = btn.Text;
            sk1 = double.Parse(Rezultats.Text);
            Rezultats.Text = "";
         }

        private void vienads_Click(object sender, EventArgs e)
        {
            
            sk2 = double.Parse(Rezultats.Text);
           
           
            switch (op)
            {
                case "+": rez = sk1 + sk2; break;
                case "-": rez = sk1 - sk2; break;
                case "*": rez = sk1 * sk2; break;
                case "/": rez = sk1 / sk2; break;
                default: rez = 0; break;
            }
           
            Rezultats.Text = rez.ToString();

        }

        
    }
}
