﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calculator;
using calkulators_17_03;
using LL_Kalkulators;
using JL_Kalkulators;
using ZanesCalculator;
using Pirma_programma;
using kalkulators;
using KalkalatorsAK;

namespace StudentuIS
{
    public partial class MusuDarbi : Form
    {
        public MusuDarbi()
        {
            InitializeComponent();
        }

        private void zanesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Zane ZaneCalc = new Zane();
            ZaneCalc.Show();
        }

        private void sergejaKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SJ_kalkulators calculus = new SJ_kalkulators();
            calculus.Show();
        }

        private void lauraKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPamats Laura_kalk = new FormPamats();
            Laura_kalk.Show();
        }

        private void jāņaFKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JF_Calculator jf_calc = new JF_Calculator();
            jf_calc.Show();
        }

        private void rihardaKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RA_Kalkulators Pirma_programma = new RA_Kalkulators();
            Pirma_programma.Show();
        }

        private void āņaLKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JLkalkulators JLkalk = new JLkalkulators();
            JLkalk.Show();
        }

        private void edvardsKalkulatorsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Edvards_kalk EdvCalc = new Edvards_kalk();
            EdvCalc.Show();
        }

        private void juraKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JT_kalkulators JT_kalkulators = new JT_kalkulators();
            JT_kalkulators.Show();
        }

        private void nikolajaKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Nikolaja_kalkulators NikolajaCalc = new Nikolaja_kalkulators();

            NikolajaCalc.Show();
        }

        private void arturaCalcToolStripMenuItem_Click(object sender, EventArgs e)
        {
            artcalc CalculatorAZ = new artcalc();
            CalculatorAZ.Show();
        }

        private void janaAKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JA_kalk JLkalk = new JA_kalk();
            JLkalk.Show();
        }

        private void iljasKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            JA_kalk JLkalk = new JA_kalk();
            JLkalk.Show();
        }

        private void līvasKalkWIPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Liva_kalk LBkalk = new Liva_kalk();
            LBkalk.Show();
        }

        private void viktoraKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Viktora_Kalk VNkalk = new Viktora_Kalk();
            VNkalk.Show();
        }

        private void mūsuKalkulatoriToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void artaKalkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 AKcalc = new Form1();
            AKcalc.Show();
        }
    }
}

