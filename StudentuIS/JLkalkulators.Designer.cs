﻿namespace JL_Kalkulators
{
    partial class JLkalkulators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Logs = new System.Windows.Forms.TextBox();
            this.Clear = new System.Windows.Forms.Button();
            this.dalits = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.plusminus = new System.Windows.Forms.Button();
            this.p9 = new System.Windows.Forms.Button();
            this.p8 = new System.Windows.Forms.Button();
            this.p7 = new System.Windows.Forms.Button();
            this.reiz = new System.Windows.Forms.Button();
            this.p6 = new System.Windows.Forms.Button();
            this.p5 = new System.Windows.Forms.Button();
            this.p4 = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.p3 = new System.Windows.Forms.Button();
            this.p2 = new System.Windows.Forms.Button();
            this.p1 = new System.Windows.Forms.Button();
            this.Rez = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.pk = new System.Windows.Forms.Button();
            this.p0 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Logs
            // 
            this.Logs.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Logs.Location = new System.Drawing.Point(12, 12);
            this.Logs.Name = "Logs";
            this.Logs.ReadOnly = true;
            this.Logs.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Logs.Size = new System.Drawing.Size(203, 30);
            this.Logs.TabIndex = 18;
            this.Logs.Text = "0";
            this.Logs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Clear
            // 
            this.Clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Clear.Location = new System.Drawing.Point(180, 87);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(35, 33);
            this.Clear.TabIndex = 17;
            this.Clear.Text = "C";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // dalits
            // 
            this.dalits.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.dalits.Location = new System.Drawing.Point(138, 48);
            this.dalits.Name = "dalits";
            this.dalits.Size = new System.Drawing.Size(35, 33);
            this.dalits.TabIndex = 15;
            this.dalits.Text = "/";
            this.dalits.UseVisualStyleBackColor = true;
            this.dalits.Click += new System.EventHandler(this.dalits_Click);
            // 
            // sqrt
            // 
            this.sqrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.sqrt.Location = new System.Drawing.Point(180, 48);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(35, 33);
            this.sqrt.TabIndex = 16;
            this.sqrt.Text = "√";
            this.sqrt.UseVisualStyleBackColor = true;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // plusminus
            // 
            this.plusminus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.plusminus.Location = new System.Drawing.Point(12, 165);
            this.plusminus.Name = "plusminus";
            this.plusminus.Size = new System.Drawing.Size(35, 33);
            this.plusminus.TabIndex = 19;
            this.plusminus.Text = "+/-";
            this.plusminus.UseVisualStyleBackColor = true;
            this.plusminus.Click += new System.EventHandler(this.plusminus_Click);
            // 
            // p9
            // 
            this.p9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p9.Location = new System.Drawing.Point(96, 48);
            this.p9.Name = "p9";
            this.p9.Size = new System.Drawing.Size(35, 33);
            this.p9.TabIndex = 11;
            this.p9.Text = "9";
            this.p9.UseVisualStyleBackColor = true;
            this.p9.Click += new System.EventHandler(this.p9_Click);
            // 
            // p8
            // 
            this.p8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p8.Location = new System.Drawing.Point(54, 48);
            this.p8.Name = "p8";
            this.p8.Size = new System.Drawing.Size(35, 33);
            this.p8.TabIndex = 10;
            this.p8.Text = "8";
            this.p8.UseVisualStyleBackColor = true;
            this.p8.Click += new System.EventHandler(this.p8_Click);
            // 
            // p7
            // 
            this.p7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p7.Location = new System.Drawing.Point(12, 48);
            this.p7.Name = "p7";
            this.p7.Size = new System.Drawing.Size(35, 33);
            this.p7.TabIndex = 9;
            this.p7.Text = "7";
            this.p7.UseVisualStyleBackColor = true;
            this.p7.Click += new System.EventHandler(this.p7_Click);
            // 
            // reiz
            // 
            this.reiz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.reiz.Location = new System.Drawing.Point(138, 87);
            this.reiz.Name = "reiz";
            this.reiz.Size = new System.Drawing.Size(35, 33);
            this.reiz.TabIndex = 14;
            this.reiz.Text = "*";
            this.reiz.UseVisualStyleBackColor = true;
            this.reiz.Click += new System.EventHandler(this.reiz_Click);
            // 
            // p6
            // 
            this.p6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p6.Location = new System.Drawing.Point(96, 87);
            this.p6.Name = "p6";
            this.p6.Size = new System.Drawing.Size(35, 33);
            this.p6.TabIndex = 8;
            this.p6.Text = "6";
            this.p6.UseVisualStyleBackColor = true;
            this.p6.Click += new System.EventHandler(this.p6_Click);
            // 
            // p5
            // 
            this.p5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p5.Location = new System.Drawing.Point(54, 87);
            this.p5.Name = "p5";
            this.p5.Size = new System.Drawing.Size(35, 33);
            this.p5.TabIndex = 7;
            this.p5.Text = "5";
            this.p5.UseVisualStyleBackColor = true;
            this.p5.Click += new System.EventHandler(this.p5_Click);
            // 
            // p4
            // 
            this.p4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p4.Location = new System.Drawing.Point(12, 87);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(35, 33);
            this.p4.TabIndex = 6;
            this.p4.Text = "4";
            this.p4.UseVisualStyleBackColor = true;
            this.p4.Click += new System.EventHandler(this.p4_Click);
            // 
            // minus
            // 
            this.minus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.minus.Location = new System.Drawing.Point(138, 126);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(35, 33);
            this.minus.TabIndex = 13;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // p3
            // 
            this.p3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p3.Location = new System.Drawing.Point(96, 126);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(35, 33);
            this.p3.TabIndex = 5;
            this.p3.Text = "3";
            this.p3.UseVisualStyleBackColor = true;
            this.p3.Click += new System.EventHandler(this.p3_Click);
            // 
            // p2
            // 
            this.p2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p2.Location = new System.Drawing.Point(54, 126);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(35, 33);
            this.p2.TabIndex = 4;
            this.p2.Text = "2";
            this.p2.UseVisualStyleBackColor = true;
            this.p2.Click += new System.EventHandler(this.p2_Click);
            // 
            // p1
            // 
            this.p1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p1.Location = new System.Drawing.Point(12, 126);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(35, 33);
            this.p1.TabIndex = 3;
            this.p1.Text = "1";
            this.p1.UseVisualStyleBackColor = true;
            this.p1.Click += new System.EventHandler(this.p1_Click);
            // 
            // Rez
            // 
            this.Rez.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Rez.Location = new System.Drawing.Point(180, 126);
            this.Rez.Name = "Rez";
            this.Rez.Size = new System.Drawing.Size(35, 72);
            this.Rez.TabIndex = 0;
            this.Rez.Text = "=";
            this.Rez.UseVisualStyleBackColor = true;
            this.Rez.Click += new System.EventHandler(this.Rez_Click);
            // 
            // plus
            // 
            this.plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.plus.Location = new System.Drawing.Point(138, 165);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(35, 33);
            this.plus.TabIndex = 12;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // pk
            // 
            this.pk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.pk.Location = new System.Drawing.Point(96, 165);
            this.pk.Name = "pk";
            this.pk.Size = new System.Drawing.Size(35, 33);
            this.pk.TabIndex = 1;
            this.pk.Text = ",";
            this.pk.UseVisualStyleBackColor = true;
            this.pk.Click += new System.EventHandler(this.pk_Click);
            // 
            // p0
            // 
            this.p0.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.p0.Location = new System.Drawing.Point(54, 165);
            this.p0.Name = "p0";
            this.p0.Size = new System.Drawing.Size(35, 33);
            this.p0.TabIndex = 2;
            this.p0.Text = "0";
            this.p0.UseVisualStyleBackColor = true;
            this.p0.Click += new System.EventHandler(this.p0_Click);
            // 
            // JLkalkulators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(231, 212);
            this.Controls.Add(this.Rez);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.pk);
            this.Controls.Add(this.p0);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.p3);
            this.Controls.Add(this.p2);
            this.Controls.Add(this.p1);
            this.Controls.Add(this.reiz);
            this.Controls.Add(this.p6);
            this.Controls.Add(this.p5);
            this.Controls.Add(this.p4);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.plusminus);
            this.Controls.Add(this.p9);
            this.Controls.Add(this.p8);
            this.Controls.Add(this.p7);
            this.Controls.Add(this.dalits);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Logs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "JLkalkulators";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "Kalkulators";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Logs;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button dalits;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Button plusminus;
        private System.Windows.Forms.Button p9;
        private System.Windows.Forms.Button p8;
        private System.Windows.Forms.Button p7;
        private System.Windows.Forms.Button reiz;
        private System.Windows.Forms.Button p6;
        private System.Windows.Forms.Button p5;
        private System.Windows.Forms.Button p4;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button p3;
        private System.Windows.Forms.Button p2;
        private System.Windows.Forms.Button p1;
        private System.Windows.Forms.Button Rez;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button pk;
        private System.Windows.Forms.Button p0;
    }
}

