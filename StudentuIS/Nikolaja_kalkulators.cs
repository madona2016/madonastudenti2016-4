﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentuIS
{
    public partial class Nikolaja_kalkulators : Form
    {
        public Nikolaja_kalkulators()
        {
            InitializeComponent();
        }

        double sk1, sk2, rez;
        string op = "";

        private void button16_Click(object sender, EventArgs e)
        {

            sk2 = double.Parse(Ekrans.Text);

            switch (op)
            {
                case "+": rez = sk1 + sk2; break;
                case "-": rez = sk1 - sk2; break;
                case "*": rez = sk1 * sk2; break;
                case "/": rez = sk1 / sk2; break;
                default: rez = 0; break;
            }

            Ekrans.Text = rez.ToString();
        }

        private void nulle_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            Ekrans.Text = Ekrans.Text + btn.Text;

        }

        private void attirit_Click(object sender, EventArgs e)
        {
            Ekrans.Clear();
        }

        private void pluss_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            op = btn.Text;
            sk1 = double.Parse(Ekrans.Text);
            Ekrans.Clear();
        }
    }
}
