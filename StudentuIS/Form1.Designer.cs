﻿namespace Kalkulators
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.t = new System.Windows.Forms.TextBox();
            this.b1 = new System.Windows.Forms.Button();
            this.b2 = new System.Windows.Forms.Button();
            this.b3 = new System.Windows.Forms.Button();
            this.b4 = new System.Windows.Forms.Button();
            this.b5 = new System.Windows.Forms.Button();
            this.b6 = new System.Windows.Forms.Button();
            this.b7 = new System.Windows.Forms.Button();
            this.b8 = new System.Windows.Forms.Button();
            this.b9 = new System.Windows.Forms.Button();
            this.b0 = new System.Windows.Forms.Button();
            this.b10 = new System.Windows.Forms.Button();
            this.b12 = new System.Windows.Forms.Button();
            this.b13 = new System.Windows.Forms.Button();
            this.b14 = new System.Windows.Forms.Button();
            this.b15 = new System.Windows.Forms.Button();
            this.b16 = new System.Windows.Forms.Button();
            this.b17 = new System.Windows.Forms.Button();
            this.b18 = new System.Windows.Forms.Button();
            this.b19 = new System.Windows.Forms.Button();
            this.b20 = new System.Windows.Forms.Button();
            this.b21 = new System.Windows.Forms.Button();
            this.b22 = new System.Windows.Forms.Button();
            this.b11 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // t
            // 
            this.t.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.t.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.t.Location = new System.Drawing.Point(76, 54);
            this.t.Margin = new System.Windows.Forms.Padding(6);
            this.t.Multiline = true;
            this.t.Name = "t";
            this.t.Size = new System.Drawing.Size(418, 106);
            this.t.TabIndex = 0;
            this.t.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.t.TextChanged += new System.EventHandler(this.t_TextChanged);
            // 
            // b1
            // 
            this.b1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b1.Location = new System.Drawing.Point(74, 423);
            this.b1.Margin = new System.Windows.Forms.Padding(6);
            this.b1.Name = "b1";
            this.b1.Size = new System.Drawing.Size(86, 75);
            this.b1.TabIndex = 1;
            this.b1.Text = "1";
            this.b1.UseVisualStyleBackColor = true;
            this.b1.Click += new System.EventHandler(this.b1_Click);
            // 
            // b2
            // 
            this.b2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b2.Location = new System.Drawing.Point(158, 423);
            this.b2.Margin = new System.Windows.Forms.Padding(6);
            this.b2.Name = "b2";
            this.b2.Size = new System.Drawing.Size(86, 75);
            this.b2.TabIndex = 2;
            this.b2.Text = "2";
            this.b2.UseVisualStyleBackColor = true;
            this.b2.Click += new System.EventHandler(this.b2_Click);
            // 
            // b3
            // 
            this.b3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b3.Location = new System.Drawing.Point(242, 423);
            this.b3.Margin = new System.Windows.Forms.Padding(6);
            this.b3.Name = "b3";
            this.b3.Size = new System.Drawing.Size(86, 75);
            this.b3.TabIndex = 3;
            this.b3.Text = "3";
            this.b3.UseVisualStyleBackColor = true;
            this.b3.Click += new System.EventHandler(this.b3_Click);
            // 
            // b4
            // 
            this.b4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b4.Location = new System.Drawing.Point(74, 337);
            this.b4.Margin = new System.Windows.Forms.Padding(6);
            this.b4.Name = "b4";
            this.b4.Size = new System.Drawing.Size(86, 75);
            this.b4.TabIndex = 4;
            this.b4.Text = "4";
            this.b4.UseVisualStyleBackColor = true;
            this.b4.Click += new System.EventHandler(this.b4_Click);
            // 
            // b5
            // 
            this.b5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b5.Location = new System.Drawing.Point(158, 337);
            this.b5.Margin = new System.Windows.Forms.Padding(6);
            this.b5.Name = "b5";
            this.b5.Size = new System.Drawing.Size(86, 75);
            this.b5.TabIndex = 5;
            this.b5.Text = "5";
            this.b5.UseVisualStyleBackColor = true;
            this.b5.Click += new System.EventHandler(this.b5_Click);
            // 
            // b6
            // 
            this.b6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b6.Location = new System.Drawing.Point(242, 337);
            this.b6.Margin = new System.Windows.Forms.Padding(6);
            this.b6.Name = "b6";
            this.b6.Size = new System.Drawing.Size(86, 75);
            this.b6.TabIndex = 6;
            this.b6.Text = "6";
            this.b6.UseVisualStyleBackColor = true;
            this.b6.Click += new System.EventHandler(this.b6_Click);
            // 
            // b7
            // 
            this.b7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b7.Location = new System.Drawing.Point(74, 250);
            this.b7.Margin = new System.Windows.Forms.Padding(6);
            this.b7.Name = "b7";
            this.b7.Size = new System.Drawing.Size(86, 75);
            this.b7.TabIndex = 7;
            this.b7.Text = "7";
            this.b7.UseVisualStyleBackColor = true;
            this.b7.Click += new System.EventHandler(this.b7_Click);
            // 
            // b8
            // 
            this.b8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b8.Location = new System.Drawing.Point(158, 250);
            this.b8.Margin = new System.Windows.Forms.Padding(6);
            this.b8.Name = "b8";
            this.b8.Size = new System.Drawing.Size(86, 75);
            this.b8.TabIndex = 8;
            this.b8.Text = "8";
            this.b8.UseVisualStyleBackColor = true;
            this.b8.Click += new System.EventHandler(this.b8_Click);
            // 
            // b9
            // 
            this.b9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b9.Location = new System.Drawing.Point(242, 250);
            this.b9.Margin = new System.Windows.Forms.Padding(6);
            this.b9.Name = "b9";
            this.b9.Size = new System.Drawing.Size(86, 75);
            this.b9.TabIndex = 9;
            this.b9.Text = "9";
            this.b9.UseVisualStyleBackColor = true;
            this.b9.Click += new System.EventHandler(this.b9_Click);
            // 
            // b0
            // 
            this.b0.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b0.Location = new System.Drawing.Point(74, 510);
            this.b0.Margin = new System.Windows.Forms.Padding(6);
            this.b0.Name = "b0";
            this.b0.Size = new System.Drawing.Size(170, 75);
            this.b0.TabIndex = 10;
            this.b0.Text = "0";
            this.b0.UseVisualStyleBackColor = true;
            this.b0.Click += new System.EventHandler(this.b0_Click);
            // 
            // b10
            // 
            this.b10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b10.Location = new System.Drawing.Point(242, 510);
            this.b10.Margin = new System.Windows.Forms.Padding(6);
            this.b10.Name = "b10";
            this.b10.Size = new System.Drawing.Size(86, 75);
            this.b10.TabIndex = 11;
            this.b10.Text = ",";
            this.b10.UseVisualStyleBackColor = true;
            this.b10.Click += new System.EventHandler(this.b10_Click);
            // 
            // b12
            // 
            this.b12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b12.Location = new System.Drawing.Point(326, 250);
            this.b12.Margin = new System.Windows.Forms.Padding(6);
            this.b12.Name = "b12";
            this.b12.Size = new System.Drawing.Size(86, 75);
            this.b12.TabIndex = 12;
            this.b12.Text = "/";
            this.b12.UseVisualStyleBackColor = true;
            this.b12.Click += new System.EventHandler(this.b12_Click);
            // 
            // b13
            // 
            this.b13.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b13.Location = new System.Drawing.Point(326, 337);
            this.b13.Margin = new System.Windows.Forms.Padding(6);
            this.b13.Name = "b13";
            this.b13.Size = new System.Drawing.Size(86, 75);
            this.b13.TabIndex = 13;
            this.b13.Text = "*";
            this.b13.UseVisualStyleBackColor = true;
            this.b13.Click += new System.EventHandler(this.b13_Click);
            // 
            // b14
            // 
            this.b14.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b14.Location = new System.Drawing.Point(326, 423);
            this.b14.Margin = new System.Windows.Forms.Padding(6);
            this.b14.Name = "b14";
            this.b14.Size = new System.Drawing.Size(86, 75);
            this.b14.TabIndex = 14;
            this.b14.Text = "-";
            this.b14.UseVisualStyleBackColor = true;
            this.b14.Click += new System.EventHandler(this.b14_Click);
            // 
            // b15
            // 
            this.b15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b15.Location = new System.Drawing.Point(326, 510);
            this.b15.Margin = new System.Windows.Forms.Padding(6);
            this.b15.Name = "b15";
            this.b15.Size = new System.Drawing.Size(86, 75);
            this.b15.TabIndex = 15;
            this.b15.Text = "+";
            this.b15.UseVisualStyleBackColor = true;
            this.b15.Click += new System.EventHandler(this.b15_Click);
            // 
            // b16
            // 
            this.b16.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b16.Location = new System.Drawing.Point(74, 163);
            this.b16.Margin = new System.Windows.Forms.Padding(6);
            this.b16.Name = "b16";
            this.b16.Size = new System.Drawing.Size(86, 75);
            this.b16.TabIndex = 16;
            this.b16.Text = "Back";
            this.b16.UseVisualStyleBackColor = true;
            this.b16.Click += new System.EventHandler(this.b16_Click);
            // 
            // b17
            // 
            this.b17.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b17.Location = new System.Drawing.Point(158, 163);
            this.b17.Margin = new System.Windows.Forms.Padding(6);
            this.b17.Name = "b17";
            this.b17.Size = new System.Drawing.Size(86, 75);
            this.b17.TabIndex = 17;
            this.b17.Text = "CE";
            this.b17.UseVisualStyleBackColor = true;
            this.b17.Click += new System.EventHandler(this.b17_Click);
            // 
            // b18
            // 
            this.b18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b18.Location = new System.Drawing.Point(242, 163);
            this.b18.Margin = new System.Windows.Forms.Padding(6);
            this.b18.Name = "b18";
            this.b18.Size = new System.Drawing.Size(86, 75);
            this.b18.TabIndex = 18;
            this.b18.Text = "C";
            this.b18.UseVisualStyleBackColor = true;
            this.b18.Click += new System.EventHandler(this.b18_Click);
            // 
            // b19
            // 
            this.b19.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b19.Location = new System.Drawing.Point(326, 163);
            this.b19.Margin = new System.Windows.Forms.Padding(6);
            this.b19.Name = "b19";
            this.b19.Size = new System.Drawing.Size(86, 75);
            this.b19.TabIndex = 19;
            this.b19.Text = "+/-";
            this.b19.UseVisualStyleBackColor = true;
            this.b19.Click += new System.EventHandler(this.b19_Click);
            // 
            // b20
            // 
            this.b20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b20.Location = new System.Drawing.Point(410, 163);
            this.b20.Margin = new System.Windows.Forms.Padding(6);
            this.b20.Name = "b20";
            this.b20.Size = new System.Drawing.Size(86, 75);
            this.b20.TabIndex = 20;
            this.b20.Text = "sqrt";
            this.b20.UseVisualStyleBackColor = true;
            this.b20.Click += new System.EventHandler(this.b20_Click);
            // 
            // b21
            // 
            this.b21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b21.Location = new System.Drawing.Point(408, 250);
            this.b21.Margin = new System.Windows.Forms.Padding(6);
            this.b21.Name = "b21";
            this.b21.Size = new System.Drawing.Size(86, 75);
            this.b21.TabIndex = 21;
            this.b21.Text = "%";
            this.b21.UseVisualStyleBackColor = true;
            // 
            // b22
            // 
            this.b22.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b22.Location = new System.Drawing.Point(410, 337);
            this.b22.Margin = new System.Windows.Forms.Padding(6);
            this.b22.Name = "b22";
            this.b22.Size = new System.Drawing.Size(86, 75);
            this.b22.TabIndex = 22;
            this.b22.Text = "1/x";
            this.b22.UseVisualStyleBackColor = true;
            this.b22.Click += new System.EventHandler(this.b22_Click);
            // 
            // b11
            // 
            this.b11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.b11.Location = new System.Drawing.Point(410, 423);
            this.b11.Margin = new System.Windows.Forms.Padding(6);
            this.b11.Name = "b11";
            this.b11.Size = new System.Drawing.Size(86, 162);
            this.b11.TabIndex = 23;
            this.b11.Text = "=";
            this.b11.UseVisualStyleBackColor = true;
            this.b11.Click += new System.EventHandler(this.b23_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(564, 650);
            this.Controls.Add(this.b11);
            this.Controls.Add(this.b22);
            this.Controls.Add(this.b21);
            this.Controls.Add(this.b20);
            this.Controls.Add(this.b19);
            this.Controls.Add(this.b18);
            this.Controls.Add(this.b17);
            this.Controls.Add(this.b16);
            this.Controls.Add(this.b15);
            this.Controls.Add(this.b14);
            this.Controls.Add(this.b13);
            this.Controls.Add(this.b12);
            this.Controls.Add(this.b10);
            this.Controls.Add(this.b0);
            this.Controls.Add(this.b9);
            this.Controls.Add(this.b8);
            this.Controls.Add(this.b7);
            this.Controls.Add(this.b6);
            this.Controls.Add(this.b5);
            this.Controls.Add(this.b4);
            this.Controls.Add(this.b3);
            this.Controls.Add(this.b2);
            this.Controls.Add(this.b1);
            this.Controls.Add(this.t);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form1";
            this.Text = "Kalkulators";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox t;
        private System.Windows.Forms.Button b1;
        private System.Windows.Forms.Button b2;
        private System.Windows.Forms.Button b3;
        private System.Windows.Forms.Button b4;
        private System.Windows.Forms.Button b5;
        private System.Windows.Forms.Button b6;
        private System.Windows.Forms.Button b7;
        private System.Windows.Forms.Button b8;
        private System.Windows.Forms.Button b9;
        private System.Windows.Forms.Button b0;
        private System.Windows.Forms.Button b10;
        private System.Windows.Forms.Button b12;
        private System.Windows.Forms.Button b13;
        private System.Windows.Forms.Button b14;
        private System.Windows.Forms.Button b15;
        private System.Windows.Forms.Button b16;
        private System.Windows.Forms.Button b17;
        private System.Windows.Forms.Button b18;
        private System.Windows.Forms.Button b19;
        private System.Windows.Forms.Button b20;
        private System.Windows.Forms.Button b21;
        private System.Windows.Forms.Button b22;
        private System.Windows.Forms.Button b11;

        public System.EventHandler button17_Click { get; set; }
    }
}

