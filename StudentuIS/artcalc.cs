﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentuIS
{
    public partial class artcalc : Form
    {
        double sk1, sk2, rez;
        string op = "";
        public artcalc()
        {
            InitializeComponent();
        }

        private void vienads_Click(object sender, EventArgs e)
        {
            sk2 = double.Parse(textBox1.Text);


            switch (op)
            {
                case "+": rez = sk1 + sk2; break;
                case "-": rez = sk1 - sk2; break;
                case "*": rez = sk1 * sk2; break;
                case "/": rez = sk1 / sk2; break;
                default: rez = 0; break;
            }
            textBox1.Text = rez.ToString();
        }
        private void poga0_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            textBox1.Text = textBox1.Text + btn.Text;
        }

        private void pogaplus_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            op = btn.Text;
            sk1 = double.Parse(textBox1.Text);
            textBox1.Text = "";
        }
    }
}
