﻿namespace Calculator
{
    partial class JF_Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBox = new System.Windows.Forms.TextBox();
            this.PlusMinus = new System.Windows.Forms.Button();
            this.zero = new System.Windows.Forms.Button();
            this.dot = new System.Windows.Forms.Button();
            this.eq = new System.Windows.Forms.Button();
            this.percent = new System.Windows.Forms.Button();
            this.one = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.div = new System.Windows.Forms.Button();
            this.multi = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.clearen = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBox
            // 
            this.TextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextBox.Location = new System.Drawing.Point(12, 12);
            this.TextBox.Multiline = true;
            this.TextBox.Name = "TextBox";
            this.TextBox.ReadOnly = true;
            this.TextBox.Size = new System.Drawing.Size(260, 24);
            this.TextBox.TabIndex = 0;
            this.TextBox.Text = "0";
            this.TextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // PlusMinus
            // 
            this.PlusMinus.Location = new System.Drawing.Point(12, 212);
            this.PlusMinus.Name = "PlusMinus";
            this.PlusMinus.Size = new System.Drawing.Size(43, 37);
            this.PlusMinus.TabIndex = 1;
            this.PlusMinus.Text = "+/-";
            this.PlusMinus.UseVisualStyleBackColor = true;
            this.PlusMinus.Click += new System.EventHandler(this.PlusMinus_Click);
            // 
            // zero
            // 
            this.zero.Location = new System.Drawing.Point(61, 212);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(43, 37);
            this.zero.TabIndex = 1;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            this.zero.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // dot
            // 
            this.dot.Location = new System.Drawing.Point(110, 212);
            this.dot.Name = "dot";
            this.dot.Size = new System.Drawing.Size(43, 37);
            this.dot.TabIndex = 1;
            this.dot.Text = ".";
            this.dot.UseVisualStyleBackColor = true;
            this.dot.Click += new System.EventHandler(this.dot_Click);
            // 
            // eq
            // 
            this.eq.Location = new System.Drawing.Point(179, 212);
            this.eq.Name = "eq";
            this.eq.Size = new System.Drawing.Size(43, 37);
            this.eq.TabIndex = 1;
            this.eq.Text = "=";
            this.eq.UseVisualStyleBackColor = true;
            this.eq.Click += new System.EventHandler(this.eq_Click);
            // 
            // percent
            // 
            this.percent.Location = new System.Drawing.Point(229, 212);
            this.percent.Name = "percent";
            this.percent.Size = new System.Drawing.Size(43, 37);
            this.percent.TabIndex = 1;
            this.percent.Text = "%";
            this.percent.UseVisualStyleBackColor = true;
            this.percent.Click += new System.EventHandler(this.percent_Click);
            // 
            // one
            // 
            this.one.Location = new System.Drawing.Point(12, 169);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(43, 37);
            this.one.TabIndex = 1;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // two
            // 
            this.two.Location = new System.Drawing.Point(61, 169);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(43, 37);
            this.two.TabIndex = 1;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // three
            // 
            this.three.Location = new System.Drawing.Point(110, 169);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(43, 37);
            this.three.TabIndex = 1;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(179, 169);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(43, 37);
            this.plus.TabIndex = 1;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(229, 169);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(43, 37);
            this.minus.TabIndex = 1;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // four
            // 
            this.four.Location = new System.Drawing.Point(12, 126);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(43, 37);
            this.four.TabIndex = 1;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // six
            // 
            this.six.Location = new System.Drawing.Point(110, 126);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(43, 37);
            this.six.TabIndex = 1;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = true;
            this.six.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // div
            // 
            this.div.Location = new System.Drawing.Point(179, 126);
            this.div.Name = "div";
            this.div.Size = new System.Drawing.Size(43, 37);
            this.div.TabIndex = 1;
            this.div.Text = "/";
            this.div.UseVisualStyleBackColor = true;
            this.div.Click += new System.EventHandler(this.div_Click);
            // 
            // multi
            // 
            this.multi.Location = new System.Drawing.Point(229, 126);
            this.multi.Name = "multi";
            this.multi.Size = new System.Drawing.Size(43, 37);
            this.multi.TabIndex = 1;
            this.multi.Text = "*";
            this.multi.UseVisualStyleBackColor = true;
            this.multi.Click += new System.EventHandler(this.multi_Click);
            // 
            // seven
            // 
            this.seven.Location = new System.Drawing.Point(12, 83);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(43, 37);
            this.seven.TabIndex = 1;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = true;
            this.seven.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // eight
            // 
            this.eight.Location = new System.Drawing.Point(61, 83);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(43, 37);
            this.eight.TabIndex = 1;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = true;
            this.eight.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // nine
            // 
            this.nine.Location = new System.Drawing.Point(110, 83);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(43, 37);
            this.nine.TabIndex = 1;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = true;
            this.nine.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // clearen
            // 
            this.clearen.Location = new System.Drawing.Point(179, 83);
            this.clearen.Name = "clearen";
            this.clearen.Size = new System.Drawing.Size(43, 37);
            this.clearen.TabIndex = 1;
            this.clearen.Text = "CE";
            this.clearen.UseVisualStyleBackColor = true;
            this.clearen.Click += new System.EventHandler(this.clearen_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(229, 83);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(43, 37);
            this.clear.TabIndex = 1;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // five
            // 
            this.five.Location = new System.Drawing.Point(61, 126);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(43, 37);
            this.five.TabIndex = 1;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.Bttn_Click);
            // 
            // JF_Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.multi);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.percent);
            this.Controls.Add(this.clearen);
            this.Controls.Add(this.div);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.eq);
            this.Controls.Add(this.nine);
            this.Controls.Add(this.six);
            this.Controls.Add(this.three);
            this.Controls.Add(this.dot);
            this.Controls.Add(this.eight);
            this.Controls.Add(this.two);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.seven);
            this.Controls.Add(this.five);
            this.Controls.Add(this.four);
            this.Controls.Add(this.one);
            this.Controls.Add(this.PlusMinus);
            this.Controls.Add(this.TextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "JF_Calculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "JF_Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBox;
        private System.Windows.Forms.Button PlusMinus;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button dot;
        private System.Windows.Forms.Button eq;
        private System.Windows.Forms.Button percent;
        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button div;
        private System.Windows.Forms.Button multi;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button clearen;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Button five;
    }
}

