﻿namespace StudentuIS
{
    partial class MusuDarbi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mūsuKalkulatoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zanesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edgaraKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sergejaKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lauraKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.āņaLKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.jāņaFKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingasKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rihardaKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artiaTKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edvardsKalkulatorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.juraKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nikolajaKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arturaCalcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.artaKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.janaAKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iljasKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.līvasKalkWIPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viktoraKalkToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mūsuKalkulatoriToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(3, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(499, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mūsuKalkulatoriToolStripMenuItem
            // 
            this.mūsuKalkulatoriToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zanesToolStripMenuItem,
            this.edgaraKalkToolStripMenuItem,
            this.sergejaKalkToolStripMenuItem,
            this.lauraKalkToolStripMenuItem,
            this.āņaLKalkToolStripMenuItem,
            this.jāņaFKalkToolStripMenuItem,
            this.ingasKalkToolStripMenuItem,
            this.rihardaKalkToolStripMenuItem,
            this.artiaTKalkToolStripMenuItem,
            this.edvardsKalkulatorsToolStripMenuItem,
            this.juraKalkToolStripMenuItem,
            this.nikolajaKalkToolStripMenuItem,
            this.arturaCalcToolStripMenuItem,
            this.artaKalkToolStripMenuItem,
            this.janaAKalkToolStripMenuItem,
            this.iljasKalkToolStripMenuItem,
            this.līvasKalkWIPToolStripMenuItem,
            this.viktoraKalkToolStripMenuItem});
            this.mūsuKalkulatoriToolStripMenuItem.Name = "mūsuKalkulatoriToolStripMenuItem";
            this.mūsuKalkulatoriToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.mūsuKalkulatoriToolStripMenuItem.Text = "Mūsu kalkulatori";
            this.mūsuKalkulatoriToolStripMenuItem.Click += new System.EventHandler(this.mūsuKalkulatoriToolStripMenuItem_Click);
            // 
            // zanesToolStripMenuItem
            // 
            this.zanesToolStripMenuItem.Name = "zanesToolStripMenuItem";
            this.zanesToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.zanesToolStripMenuItem.Text = "Zanes kalk.";
            this.zanesToolStripMenuItem.Click += new System.EventHandler(this.zanesToolStripMenuItem_Click);
            // 
            // edgaraKalkToolStripMenuItem
            // 
            this.edgaraKalkToolStripMenuItem.Name = "edgaraKalkToolStripMenuItem";
            this.edgaraKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.edgaraKalkToolStripMenuItem.Text = "Edgara kalk.";
            // 
            // sergejaKalkToolStripMenuItem
            // 
            this.sergejaKalkToolStripMenuItem.Name = "sergejaKalkToolStripMenuItem";
            this.sergejaKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.sergejaKalkToolStripMenuItem.Text = "Sergeja kalk.";
            this.sergejaKalkToolStripMenuItem.Click += new System.EventHandler(this.sergejaKalkToolStripMenuItem_Click);
            // 
            // lauraKalkToolStripMenuItem
            // 
            this.lauraKalkToolStripMenuItem.Name = "lauraKalkToolStripMenuItem";
            this.lauraKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.lauraKalkToolStripMenuItem.Text = "Laura kalk.";
            this.lauraKalkToolStripMenuItem.Click += new System.EventHandler(this.lauraKalkToolStripMenuItem_Click);
            // 
            // āņaLKalkToolStripMenuItem
            // 
            this.āņaLKalkToolStripMenuItem.Name = "āņaLKalkToolStripMenuItem";
            this.āņaLKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.āņaLKalkToolStripMenuItem.Text = "JāņaL kalk.";
            this.āņaLKalkToolStripMenuItem.Click += new System.EventHandler(this.āņaLKalkToolStripMenuItem_Click);
            // 
            // jāņaFKalkToolStripMenuItem
            // 
            this.jāņaFKalkToolStripMenuItem.Name = "jāņaFKalkToolStripMenuItem";
            this.jāņaFKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.jāņaFKalkToolStripMenuItem.Text = "JāņaF kalk.";
            this.jāņaFKalkToolStripMenuItem.Click += new System.EventHandler(this.jāņaFKalkToolStripMenuItem_Click);
            // 
            // ingasKalkToolStripMenuItem
            // 
            this.ingasKalkToolStripMenuItem.Name = "ingasKalkToolStripMenuItem";
            this.ingasKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.ingasKalkToolStripMenuItem.Text = "Ingas kalk.";
            // 
            // rihardaKalkToolStripMenuItem
            // 
            this.rihardaKalkToolStripMenuItem.Name = "rihardaKalkToolStripMenuItem";
            this.rihardaKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.rihardaKalkToolStripMenuItem.Text = "Riharda kalk.";
            this.rihardaKalkToolStripMenuItem.Click += new System.EventHandler(this.rihardaKalkToolStripMenuItem_Click);
            // 
            // artiaTKalkToolStripMenuItem
            // 
            this.artiaTKalkToolStripMenuItem.Name = "artiaTKalkToolStripMenuItem";
            this.artiaTKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.artiaTKalkToolStripMenuItem.Text = "ArtaT kalk.";
            // 
            // edvardsKalkulatorsToolStripMenuItem
            // 
            this.edvardsKalkulatorsToolStripMenuItem.Name = "edvardsKalkulatorsToolStripMenuItem";
            this.edvardsKalkulatorsToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.edvardsKalkulatorsToolStripMenuItem.Text = "Edvards kalkulators";
            this.edvardsKalkulatorsToolStripMenuItem.Click += new System.EventHandler(this.edvardsKalkulatorsToolStripMenuItem_Click);
            // 
            // juraKalkToolStripMenuItem
            // 
            this.juraKalkToolStripMenuItem.Name = "juraKalkToolStripMenuItem";
            this.juraKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.juraKalkToolStripMenuItem.Text = "Jura kalk.";
            this.juraKalkToolStripMenuItem.Click += new System.EventHandler(this.juraKalkToolStripMenuItem_Click);
            // 
            // nikolajaKalkToolStripMenuItem
            // 
            this.nikolajaKalkToolStripMenuItem.Name = "nikolajaKalkToolStripMenuItem";
            this.nikolajaKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.nikolajaKalkToolStripMenuItem.Text = "Nikolaja kalk.";
            this.nikolajaKalkToolStripMenuItem.Click += new System.EventHandler(this.nikolajaKalkToolStripMenuItem_Click);
            // 
            // arturaCalcToolStripMenuItem
            // 
            this.arturaCalcToolStripMenuItem.Name = "arturaCalcToolStripMenuItem";
            this.arturaCalcToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.arturaCalcToolStripMenuItem.Text = "Artura calc";
            this.arturaCalcToolStripMenuItem.Click += new System.EventHandler(this.arturaCalcToolStripMenuItem_Click);
            // 
            // artaKalkToolStripMenuItem
            // 
            this.artaKalkToolStripMenuItem.Name = "artaKalkToolStripMenuItem";
            this.artaKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.artaKalkToolStripMenuItem.Text = "Arta kalk.";
            this.artaKalkToolStripMenuItem.Click += new System.EventHandler(this.artaKalkToolStripMenuItem_Click);
            // 
            // janaAKalkToolStripMenuItem
            // 
            this.janaAKalkToolStripMenuItem.Name = "janaAKalkToolStripMenuItem";
            this.janaAKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.janaAKalkToolStripMenuItem.Text = "JanaA kalk.";
            this.janaAKalkToolStripMenuItem.Click += new System.EventHandler(this.janaAKalkToolStripMenuItem_Click);
            // 
            // iljasKalkToolStripMenuItem
            // 
            this.iljasKalkToolStripMenuItem.Name = "iljasKalkToolStripMenuItem";
            this.iljasKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.iljasKalkToolStripMenuItem.Text = "Iljas kalk.";
            this.iljasKalkToolStripMenuItem.Click += new System.EventHandler(this.iljasKalkToolStripMenuItem_Click);
            // 
            // līvasKalkWIPToolStripMenuItem
            // 
            this.līvasKalkWIPToolStripMenuItem.Name = "līvasKalkWIPToolStripMenuItem";
            this.līvasKalkWIPToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.līvasKalkWIPToolStripMenuItem.Text = "Līvas kalk.(WIP)";
            this.līvasKalkWIPToolStripMenuItem.Click += new System.EventHandler(this.līvasKalkWIPToolStripMenuItem_Click);
            // 
            // viktoraKalkToolStripMenuItem
            // 
            this.viktoraKalkToolStripMenuItem.Name = "viktoraKalkToolStripMenuItem";
            this.viktoraKalkToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.viktoraKalkToolStripMenuItem.Text = "Viktora kalk.";
            this.viktoraKalkToolStripMenuItem.Click += new System.EventHandler(this.viktoraKalkToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.label1.Location = new System.Drawing.Point(12, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(485, 23);
            this.label1.TabIndex = 3;
            this.label1.Text = "Lai apskatītu kalkulātorus, piemiedzat uz \"Mūsu kalkulatori\"";
            // 
            // MusuDarbi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 98);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MusuDarbi";
            this.Text = "Studentu kalkulatori";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mūsuKalkulatoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zanesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edgaraKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sergejaKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lauraKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem āņaLKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem jāņaFKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingasKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rihardaKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artiaTKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem artaKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edvardsKalkulatorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem juraKalkToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem nikolajaKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arturaCalcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem janaAKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iljasKalkToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem līvasKalkWIPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viktoraKalkToolStripMenuItem;
    }
}

