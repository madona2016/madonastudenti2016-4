﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZanesCalculator
{
    public partial class Zane : Form
    {
        Double value=0;
        String operation="";      //blank
        bool operation_pressed = false;
        public Zane()
        {
            InitializeComponent();
        }
        class Operators
        {
            public static Double Add(Double number1, Double number2)
            {
                return number1 + number2;
            }

            public static Double Div(Double number1, Double number2)
            {
                return number1 / number2;
            }
        }

        private void button_click(object sender, EventArgs e)
        {
            if ((textBox_result.Text == "0")||(operation_pressed))  //is true
                textBox_result.Clear();

            operation_pressed = false;
            Button button = (Button)sender;
            if(button.Text==",")    //limit points
            {
                if (!textBox_result.Text.Contains(","))
                    textBox_result.Text = textBox_result.Text + button.Text;
            }
            else
            textBox_result.Text = textBox_result.Text + button.Text;
            label1.Focus(); //did pass the focus to label1
        }

        private void operator_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (value != 0) //don't need press = everytime
            {
                equal.PerformClick();    //emulates = button press
                operation_pressed = true;
                operation = button.Text;
                labelCurrentOperation.Text = value + "" + operation;
            }
            else
            {
                operation = button.Text;
                value = Double.Parse(textBox_result.Text);
                labelCurrentOperation.Text = value + "" + operation;
                operation_pressed = true;
            }
            label1.Focus();
        }

        private void button5_Click(object sender, EventArgs e)  //<-- Backspace
        {
            int length = textBox_result.TextLength - 1;
            string text = textBox_result.Text;
            textBox_result.Clear();
            for (int i = 0; i < length; i++)
                textBox_result.Text = textBox_result.Text + text[i];
            label1.Focus();
        }

        private void button6_Click(object sender, EventArgs e)  //C clear
        {
            textBox_result.Text = "0";
            value = 0;  //visu notiira
            labelCurrentOperation.Text = "";
            label1.Focus();
        }

        private void button11_Click(object sender, EventArgs e) //=
        {
            labelCurrentOperation.Text = "";
                switch (operation)
                {
                    case "+":   //using class
                                //textBox_result.Text = (value + Double.Parse(textBox_result.Text)).ToString();
                        textBox_result.Text = Operators.Add(value, Double.Parse(textBox_result.Text)).ToString();
                        break;
                    case "-":
                        textBox_result.Text = (value - Double.Parse(textBox_result.Text)).ToString();
                        break;
                    case "*":
                        textBox_result.Text = (value * Double.Parse(textBox_result.Text)).ToString();
                        break;
                    case "/":   //using class
                        //textBox_result.Text = (value / Double.Parse(textBox_result.Text)).ToString();
                        textBox_result.Text = Operators.Div(value, Double.Parse(textBox_result.Text)).ToString();
                        break;
                    default:    //this should never happen, but..
                        break;
                }//end switch
            value = Double.Parse(textBox_result.Text);
            operation = "";
            label1.Focus();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch(e.KeyChar.ToString())
            {
                case "0":
                    zero.PerformClick();
                    break;
                case "1":
                    one.PerformClick();
                    break;
                case "2":
                    two.PerformClick();
                    break;
                case "3":
                    three.PerformClick();
                    break;
                case "4":
                    four.PerformClick();
                    break;
                case "5":
                    five.PerformClick();
                    break;
                case "6":
                    six.PerformClick();
                    break;
                case "7":
                    seven.PerformClick();
                    break;
                case "8":
                    eight.PerformClick();
                    break;
                case "9":
                    nine.PerformClick();
                    break;
                case "+":
                    add.PerformClick();
                    break;
                case "-":
                    sub.PerformClick();
                    break;
                case "*":
                    times.PerformClick();
                    break;
                case "/":
                    div.PerformClick();
                    break;
                case "=":
                    equal.PerformClick();
                    break;
                case ",":
                    dec.PerformClick();
                    break;
                case " ":   //when space button is pressed
                    equal.PerformClick();
                    break;
                case "\r":  //ENTER (return) button
                    equal.PerformClick();
                    break;
                default:
                    break;
            }
        }
    }
}
