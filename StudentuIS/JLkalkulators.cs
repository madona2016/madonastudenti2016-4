﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JL_Kalkulators
{
    public partial class JLkalkulators : Form
    {
        public JLkalkulators()
        {
            InitializeComponent();
        }
        public double val1 = 0, val2 = 0, rez;
        public string op = "";
        public bool rez_izvadits;

        public double darbiba (string darb)   //funkcija, lai varetu veikt vairakas darbibas pirms = zimes spiešanas
        {
            switch (darb)
            {
                case "+": rez = rez + val2; break;
                case "-": rez = rez - val2; break;
                case "*": rez = rez * val2; break;
                case "/":
                    if (val2 == 0)
                        throw new System.DivideByZeroException();
                    else
                        rez = rez / val2; break;
                default: rez = val2; break;  //ja tiek veiktas darbība pēc darbības pirmajā reizē operatora nav
            }
            return rez;
        }
        private void p1_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits) //parbauda, vai logā nav 0 vai arī iepriekšējais rezultāts
            {                                     //lai neveidotos skaitlis: 0111...; 541111... utt.
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "1"; //pievieno logam vertibu
        }

        private void p2_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "2";
        }

        private void p3_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "3";
        }

        private void p4_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "4";
        }

        private void p5_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "5";
        }

        private void p6_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "6";
        }

        private void p7_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "7";
        }

        private void p0_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "0";
        }

        private void p8_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "8";
        }

        private void p9_Click(object sender, EventArgs e)
        {
            if (Logs.Text == "0" || rez_izvadits)
            {
                Logs.Text = "";
                rez_izvadits = false;
            }
            Logs.Text = Logs.Text + "9";
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            Logs.Text = "0";  //notira logu un atiestata vertibas uz sakotnējām
            val1 = 0;
            val2 = 0;
            rez = 0;
            op = "";
            rez_izvadits = true;
        }

        private void plusminus_Click(object sender, EventArgs e)
        {
            val1 = Convert.ToDouble(Logs.Text);
            val1 = val1 * (-1);
            Logs.Text = val1.ToString();
        }

        private void dalits_Click(object sender, EventArgs e)
        {
            if (Logs.Text != "0")
            {
                val2 = Convert.ToDouble(Logs.Text);  //nolasa no loga vertibu
                darbiba(op);  //izsauc funkciju
                op = "/";  //nomaina operatora zimi
                Logs.Text = "0"; //notira logu lai varetu ievadit nakamo skaitli
            }
            else
            {
                val1 = Convert.ToDouble(Logs.Text);
                op = "/";  //ja logā ir 0 tad pirmā darbība tiks veikta ar nulli
                Logs.Text = "0";
            }
        }

        private void reiz_Click(object sender, EventArgs e)
        {
            if (Logs.Text != "0")
            {
                val2 = Convert.ToDouble(Logs.Text);
                darbiba(op);
                op = "*";
                Logs.Text = "0";
            }
            else
            {
                val1 = Convert.ToDouble(Logs.Text);
                op = "*";
                Logs.Text = "0";
            }
        }

        private void minus_Click(object sender, EventArgs e)
        {
            if (Logs.Text != "0")
            {
                val2 = Convert.ToDouble(Logs.Text);
                darbiba(op);
                op = "-";
                Logs.Text = "0";
            }
            else
            {
                val1 = Convert.ToDouble(Logs.Text);
                op = "-";
                Logs.Text = "0";
            }
        }

        private void plus_Click(object sender, EventArgs e)
        {
            if (Logs.Text != "0")
            {
                val2 = Convert.ToDouble(Logs.Text);
                darbiba(op);
                op = "+";
                Logs.Text = "0";
            }
            else
            {
                val1 = Convert.ToDouble(Logs.Text);
                op = "+";
                Logs.Text = "0";
            }
        }

        private void sqrt_Click(object sender, EventArgs e)
        {
            val1 = Convert.ToDouble(Logs.Text);
            rez = Math.Sqrt(val1);
            Logs.Text = rez.ToString();
        }

        private void Rez_Click(object sender, EventArgs e)
        {
            try
            {
                val2 = Convert.ToDouble(Logs.Text);
                Logs.Text = "";
                darbiba(op);
                op = "";
                val1 = 0;
                val2 = 0;
                Logs.Text = rez.ToString();
                rez = 0;
                rez_izvadits = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Clear.PerformClick();
            }

            //catch (DivideByZeroException ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
        }

        private void pk_Click(object sender, EventArgs e)
        {
            if(!Logs.Text.Contains(","))
                Logs.Text = Logs.Text + ",";
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar >='0' && e.KeyChar <='9')
            {
                if (Logs.Text == "0")
                    Logs.Text = "";
                Logs.Text = Logs.Text + e.KeyChar;
            }
            if (e.KeyChar == '+')
                plus_Click(sender, e);
            if (e.KeyChar == '-')
                minus_Click(sender, e);
            if (e.KeyChar == '*')
                reiz_Click(sender, e);
            if (e.KeyChar == '/')
                dalits_Click(sender, e);
            if (e.KeyChar == (char)Keys.Enter)
                Rez_Click(sender, e);
        }
    }
}
