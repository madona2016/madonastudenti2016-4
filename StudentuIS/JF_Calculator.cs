﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class JF_Calculator : Form
    {


        public JF_Calculator()
        {
            InitializeComponent();
        }
        private decimal FirstNum = 0.0m;
        private decimal SecondNum = 0.0m;
        private decimal result = 0.0m;
        private int operatorType = (int)MathOperation.NoOperation;
        private enum MathOperation
        {
            NoOperation = 0,
            Add = 1,
            Minus = 2,
            Divide = 3,
            Multiply = 4,
            Percentage = 5

        }
        private void Bttn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            if (TextBox.Text == "0")
            {
                TextBox.Clear();
            }
            TextBox.Text += btn.Text;
        }

        private void dot_Click(object sender, EventArgs e)
        {
            if (!TextBox.Text.Contains('.'))
                TextBox.Text += ".";
        }

        private void PlusMinus_Click(object sender, EventArgs e)
        {
            if (!TextBox.Text.Contains('-'))
            {
                TextBox.Text = "-" + TextBox.Text;
            }
            else
            {
                TextBox.Text = TextBox.Text.Trim('-');
            }
        }
        private void SaveValueAndOperatorType(int operation)
        {
            operatorType = operation;
            FirstNum = Convert.ToDecimal(TextBox.Text);
            TextBox.Text = "0";
        }
        private void div_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperation.Divide);
        }

        private void plus_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperation.Add);
        }

        private void minus_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperation.Minus);
        }


        private void multi_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperation.Multiply);
        }

        private void percent_Click(object sender, EventArgs e)
        {
            SaveValueAndOperatorType((int)MathOperation.Percentage);
        }

        private void eq_Click(object sender, EventArgs e)
        {
            try
            {
                SecondNum = Convert.ToDecimal(TextBox.Text);
            }
            catch(System.FormatException ex)
            {
                MessageBox.Show(ex.Message);
            }

            switch (operatorType)
            {
                case (int)MathOperation.Add:
                    result = FirstNum + SecondNum;
                    break;
                case (int)MathOperation.Minus:
                    result = FirstNum - SecondNum;
                    break;
                case (int)MathOperation.Divide:
                    try
                    {
                        result = FirstNum / SecondNum;
                    }
                    catch (DivideByZeroException ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    break;
                case (int)MathOperation.Multiply:
                    result = FirstNum * SecondNum;
                    break;
                case (int)MathOperation.Percentage:
                    result = (FirstNum / SecondNum)*100;
                    break;
               
            }
            TextBox.Text = result.ToString();
        }

        private void clearen_Click(object sender, EventArgs e)
        {
            TextBox.Text = "0";
        }

        private void clear_Click(object sender, EventArgs e)
        {
            TextBox.Text = "0";
            FirstNum = 0.0m;
            SecondNum = 0.0m;
            result = 0;
            operatorType = (int)MathOperation.Percentage;

        }
    }
}