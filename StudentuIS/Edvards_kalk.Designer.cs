﻿namespace kalkulators
{
    partial class Edvards_kalk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.vienads = new System.Windows.Forms.Button();
            this.saskaitisana = new System.Windows.Forms.Button();
            this.poga0 = new System.Windows.Forms.Button();
            this.poga1 = new System.Windows.Forms.Button();
            this.poga2 = new System.Windows.Forms.Button();
            this.poga3 = new System.Windows.Forms.Button();
            this.minuss = new System.Windows.Forms.Button();
            this.poga4 = new System.Windows.Forms.Button();
            this.poga5 = new System.Windows.Forms.Button();
            this.poga6 = new System.Windows.Forms.Button();
            this.reizinasana = new System.Windows.Forms.Button();
            this.poga7 = new System.Windows.Forms.Button();
            this.poga8 = new System.Windows.Forms.Button();
            this.poga9 = new System.Windows.Forms.Button();
            this.dalisana = new System.Windows.Forms.Button();
            this.Rezultats = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // vienads
            // 
            this.vienads.Location = new System.Drawing.Point(176, 211);
            this.vienads.Name = "vienads";
            this.vienads.Size = new System.Drawing.Size(96, 23);
            this.vienads.TabIndex = 0;
            this.vienads.Text = "=";
            this.vienads.UseVisualStyleBackColor = true;
            this.vienads.Click += new System.EventHandler(this.vienads_Click);
            // 
            // saskaitisana
            // 
            this.saskaitisana.Location = new System.Drawing.Point(108, 211);
            this.saskaitisana.Name = "saskaitisana";
            this.saskaitisana.Size = new System.Drawing.Size(48, 23);
            this.saskaitisana.TabIndex = 1;
            this.saskaitisana.Text = "+";
            this.saskaitisana.UseVisualStyleBackColor = true;
            this.saskaitisana.Click += new System.EventHandler(this.minuss_Click);
            // 
            // poga0
            // 
            this.poga0.Location = new System.Drawing.Point(43, 211);
            this.poga0.Name = "poga0";
            this.poga0.Size = new System.Drawing.Size(55, 23);
            this.poga0.TabIndex = 2;
            this.poga0.Text = "0";
            this.poga0.UseVisualStyleBackColor = true;
            this.poga0.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga1
            // 
            this.poga1.Location = new System.Drawing.Point(46, 162);
            this.poga1.Name = "poga1";
            this.poga1.Size = new System.Drawing.Size(52, 23);
            this.poga1.TabIndex = 3;
            this.poga1.Text = "1";
            this.poga1.UseVisualStyleBackColor = true;
            this.poga1.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga2
            // 
            this.poga2.Location = new System.Drawing.Point(108, 162);
            this.poga2.Name = "poga2";
            this.poga2.Size = new System.Drawing.Size(48, 23);
            this.poga2.TabIndex = 4;
            this.poga2.Text = "2";
            this.poga2.UseVisualStyleBackColor = true;
            this.poga2.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga3
            // 
            this.poga3.Location = new System.Drawing.Point(176, 162);
            this.poga3.Name = "poga3";
            this.poga3.Size = new System.Drawing.Size(32, 23);
            this.poga3.TabIndex = 5;
            this.poga3.Text = "3";
            this.poga3.UseVisualStyleBackColor = true;
            this.poga3.Click += new System.EventHandler(this.poga0_Click);
            // 
            // minuss
            // 
            this.minuss.Location = new System.Drawing.Point(227, 162);
            this.minuss.Name = "minuss";
            this.minuss.Size = new System.Drawing.Size(33, 23);
            this.minuss.TabIndex = 6;
            this.minuss.Text = "-";
            this.minuss.UseVisualStyleBackColor = true;
            this.minuss.Click += new System.EventHandler(this.minuss_Click);
            // 
            // poga4
            // 
            this.poga4.Location = new System.Drawing.Point(46, 122);
            this.poga4.Name = "poga4";
            this.poga4.Size = new System.Drawing.Size(52, 23);
            this.poga4.TabIndex = 7;
            this.poga4.Text = "4";
            this.poga4.UseVisualStyleBackColor = true;
            this.poga4.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga5
            // 
            this.poga5.Location = new System.Drawing.Point(108, 122);
            this.poga5.Name = "poga5";
            this.poga5.Size = new System.Drawing.Size(48, 23);
            this.poga5.TabIndex = 8;
            this.poga5.Text = "5";
            this.poga5.UseVisualStyleBackColor = true;
            this.poga5.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga6
            // 
            this.poga6.Location = new System.Drawing.Point(176, 122);
            this.poga6.Name = "poga6";
            this.poga6.Size = new System.Drawing.Size(32, 23);
            this.poga6.TabIndex = 9;
            this.poga6.Text = "6";
            this.poga6.UseVisualStyleBackColor = true;
            this.poga6.Click += new System.EventHandler(this.poga0_Click);
            // 
            // reizinasana
            // 
            this.reizinasana.Location = new System.Drawing.Point(227, 122);
            this.reizinasana.Name = "reizinasana";
            this.reizinasana.Size = new System.Drawing.Size(33, 23);
            this.reizinasana.TabIndex = 10;
            this.reizinasana.Text = "*";
            this.reizinasana.UseVisualStyleBackColor = true;
            this.reizinasana.Click += new System.EventHandler(this.minuss_Click);
            // 
            // poga7
            // 
            this.poga7.Location = new System.Drawing.Point(46, 80);
            this.poga7.Name = "poga7";
            this.poga7.Size = new System.Drawing.Size(52, 23);
            this.poga7.TabIndex = 11;
            this.poga7.Text = "7";
            this.poga7.UseVisualStyleBackColor = true;
            this.poga7.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga8
            // 
            this.poga8.Location = new System.Drawing.Point(109, 80);
            this.poga8.Name = "poga8";
            this.poga8.Size = new System.Drawing.Size(47, 23);
            this.poga8.TabIndex = 12;
            this.poga8.Text = "8";
            this.poga8.UseVisualStyleBackColor = true;
            this.poga8.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga9
            // 
            this.poga9.Location = new System.Drawing.Point(176, 80);
            this.poga9.Name = "poga9";
            this.poga9.Size = new System.Drawing.Size(32, 23);
            this.poga9.TabIndex = 13;
            this.poga9.Text = "9";
            this.poga9.UseVisualStyleBackColor = true;
            this.poga9.Click += new System.EventHandler(this.poga0_Click);
            // 
            // dalisana
            // 
            this.dalisana.Location = new System.Drawing.Point(227, 80);
            this.dalisana.Name = "dalisana";
            this.dalisana.Size = new System.Drawing.Size(32, 23);
            this.dalisana.TabIndex = 14;
            this.dalisana.Text = "/";
            this.dalisana.UseVisualStyleBackColor = true;
            this.dalisana.Click += new System.EventHandler(this.minuss_Click);
            // 
            // Rezultats
            // 
            this.Rezultats.Location = new System.Drawing.Point(55, 33);
            this.Rezultats.Name = "Rezultats";
            this.Rezultats.ReadOnly = true;
            this.Rezultats.Size = new System.Drawing.Size(138, 20);
            this.Rezultats.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Rezultats);
            this.Controls.Add(this.dalisana);
            this.Controls.Add(this.poga9);
            this.Controls.Add(this.poga8);
            this.Controls.Add(this.poga7);
            this.Controls.Add(this.reizinasana);
            this.Controls.Add(this.poga6);
            this.Controls.Add(this.poga5);
            this.Controls.Add(this.poga4);
            this.Controls.Add(this.minuss);
            this.Controls.Add(this.poga3);
            this.Controls.Add(this.poga2);
            this.Controls.Add(this.poga1);
            this.Controls.Add(this.poga0);
            this.Controls.Add(this.saskaitisana);
            this.Controls.Add(this.vienads);
            this.Name = "Form1";
            this.Text = "Kalkulators";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button vienads;
        private System.Windows.Forms.Button saskaitisana;
        private System.Windows.Forms.Button poga0;
        private System.Windows.Forms.Button poga1;
        private System.Windows.Forms.Button poga2;
        private System.Windows.Forms.Button poga3;
        private System.Windows.Forms.Button minuss;
        private System.Windows.Forms.Button poga4;
        private System.Windows.Forms.Button poga5;
        private System.Windows.Forms.Button poga6;
        private System.Windows.Forms.Button reizinasana;
        private System.Windows.Forms.Button poga7;
        private System.Windows.Forms.Button poga8;
        private System.Windows.Forms.Button poga9;
        private System.Windows.Forms.Button dalisana;
        private System.Windows.Forms.TextBox Rezultats;

    }
}

