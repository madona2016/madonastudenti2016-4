﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StudentuIS
{
    public partial class JT_kalkulators : Form
    {
        public JT_kalkulators()
        {
            InitializeComponent();
        }

        private void calculate_Click(object sender, EventArgs e)
        {
            switch(EditMathOperator.Text)
            {
                case "+":
                    EditResult.Text = Convert.ToString(Convert.ToDouble(EditNumber_1.Text) + Convert.ToDouble(EditNumber_2.Text));
                    break;
                case "-":
                    EditResult.Text = Convert.ToString(Convert.ToDouble(EditNumber_1.Text) - Convert.ToDouble(EditNumber_2.Text));
                    break;
                case "/":
                    EditResult.Text = Convert.ToString(Convert.ToDouble(EditNumber_1.Text) / Convert.ToDouble(EditNumber_2.Text));
                    break;
                case "*":
                    EditResult.Text = Convert.ToString(Convert.ToDouble(EditNumber_1.Text) * Convert.ToDouble(EditNumber_2.Text));
                    break;
                default:
                    EditResult.Text = Convert.ToString(Convert.ToDouble(EditNumber_1.Text) + Convert.ToDouble(EditNumber_2.Text));
                    break;
            }
        }
    }
}
