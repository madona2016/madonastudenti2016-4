﻿namespace StudentuIS
{
    partial class artcalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.vienads = new System.Windows.Forms.Button();
            this.minuss = new System.Windows.Forms.Button();
            this.pogareiz = new System.Windows.Forms.Button();
            this.pogadal = new System.Windows.Forms.Button();
            this.pogaplus = new System.Windows.Forms.Button();
            this.poga0 = new System.Windows.Forms.Button();
            this.poga3 = new System.Windows.Forms.Button();
            this.poga9 = new System.Windows.Forms.Button();
            this.poga6 = new System.Windows.Forms.Button();
            this.poga2 = new System.Windows.Forms.Button();
            this.poga5 = new System.Windows.Forms.Button();
            this.poga8 = new System.Windows.Forms.Button();
            this.poga1 = new System.Windows.Forms.Button();
            this.poga4 = new System.Windows.Forms.Button();
            this.poga7 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(40, 26);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(146, 20);
            this.textBox1.TabIndex = 33;
            // 
            // vienads
            // 
            this.vienads.Location = new System.Drawing.Point(151, 212);
            this.vienads.Name = "vienads";
            this.vienads.Size = new System.Drawing.Size(93, 23);
            this.vienads.TabIndex = 32;
            this.vienads.Text = "=";
            this.vienads.UseVisualStyleBackColor = true;
            this.vienads.Click += new System.EventHandler(this.vienads_Click);
            // 
            // minuss
            // 
            this.minuss.Location = new System.Drawing.Point(209, 167);
            this.minuss.Name = "minuss";
            this.minuss.Size = new System.Drawing.Size(35, 23);
            this.minuss.TabIndex = 31;
            this.minuss.Text = "-";
            this.minuss.UseVisualStyleBackColor = true;
            this.minuss.Click += new System.EventHandler(this.pogaplus_Click);
            // 
            // pogareiz
            // 
            this.pogareiz.Location = new System.Drawing.Point(209, 125);
            this.pogareiz.Name = "pogareiz";
            this.pogareiz.Size = new System.Drawing.Size(35, 23);
            this.pogareiz.TabIndex = 30;
            this.pogareiz.Text = "*";
            this.pogareiz.UseVisualStyleBackColor = true;
            this.pogareiz.Click += new System.EventHandler(this.pogaplus_Click);
            // 
            // pogadal
            // 
            this.pogadal.Location = new System.Drawing.Point(209, 85);
            this.pogadal.Name = "pogadal";
            this.pogadal.Size = new System.Drawing.Size(35, 23);
            this.pogadal.TabIndex = 29;
            this.pogadal.Text = "/";
            this.pogadal.UseVisualStyleBackColor = true;
            this.pogadal.Click += new System.EventHandler(this.pogaplus_Click);
            // 
            // pogaplus
            // 
            this.pogaplus.Location = new System.Drawing.Point(95, 212);
            this.pogaplus.Name = "pogaplus";
            this.pogaplus.Size = new System.Drawing.Size(35, 23);
            this.pogaplus.TabIndex = 28;
            this.pogaplus.Text = "+";
            this.pogaplus.UseVisualStyleBackColor = true;
            this.pogaplus.Click += new System.EventHandler(this.pogaplus_Click);
            // 
            // poga0
            // 
            this.poga0.Location = new System.Drawing.Point(40, 212);
            this.poga0.Name = "poga0";
            this.poga0.Size = new System.Drawing.Size(35, 23);
            this.poga0.TabIndex = 27;
            this.poga0.Text = "0";
            this.poga0.UseVisualStyleBackColor = true;
            this.poga0.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga3
            // 
            this.poga3.Location = new System.Drawing.Point(151, 167);
            this.poga3.Name = "poga3";
            this.poga3.Size = new System.Drawing.Size(35, 23);
            this.poga3.TabIndex = 26;
            this.poga3.Text = "3";
            this.poga3.UseVisualStyleBackColor = true;
            this.poga3.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga9
            // 
            this.poga9.Location = new System.Drawing.Point(151, 85);
            this.poga9.Name = "poga9";
            this.poga9.Size = new System.Drawing.Size(35, 23);
            this.poga9.TabIndex = 25;
            this.poga9.Text = "9";
            this.poga9.UseVisualStyleBackColor = true;
            this.poga9.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga6
            // 
            this.poga6.Location = new System.Drawing.Point(151, 125);
            this.poga6.Name = "poga6";
            this.poga6.Size = new System.Drawing.Size(35, 23);
            this.poga6.TabIndex = 24;
            this.poga6.Text = "6";
            this.poga6.UseVisualStyleBackColor = true;
            this.poga6.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga2
            // 
            this.poga2.Location = new System.Drawing.Point(95, 167);
            this.poga2.Name = "poga2";
            this.poga2.Size = new System.Drawing.Size(35, 23);
            this.poga2.TabIndex = 23;
            this.poga2.Text = "2";
            this.poga2.UseVisualStyleBackColor = true;
            this.poga2.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga5
            // 
            this.poga5.Location = new System.Drawing.Point(95, 125);
            this.poga5.Name = "poga5";
            this.poga5.Size = new System.Drawing.Size(35, 23);
            this.poga5.TabIndex = 22;
            this.poga5.Text = "5";
            this.poga5.UseVisualStyleBackColor = true;
            this.poga5.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga8
            // 
            this.poga8.Location = new System.Drawing.Point(95, 85);
            this.poga8.Name = "poga8";
            this.poga8.Size = new System.Drawing.Size(35, 23);
            this.poga8.TabIndex = 21;
            this.poga8.Text = "8";
            this.poga8.UseVisualStyleBackColor = true;
            this.poga8.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga1
            // 
            this.poga1.Location = new System.Drawing.Point(40, 167);
            this.poga1.Name = "poga1";
            this.poga1.Size = new System.Drawing.Size(35, 23);
            this.poga1.TabIndex = 20;
            this.poga1.Text = "1";
            this.poga1.UseVisualStyleBackColor = true;
            this.poga1.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga4
            // 
            this.poga4.Location = new System.Drawing.Point(41, 125);
            this.poga4.Name = "poga4";
            this.poga4.Size = new System.Drawing.Size(35, 23);
            this.poga4.TabIndex = 19;
            this.poga4.Text = "4";
            this.poga4.UseVisualStyleBackColor = true;
            this.poga4.Click += new System.EventHandler(this.poga0_Click);
            // 
            // poga7
            // 
            this.poga7.Location = new System.Drawing.Point(40, 85);
            this.poga7.Name = "poga7";
            this.poga7.Size = new System.Drawing.Size(35, 23);
            this.poga7.TabIndex = 18;
            this.poga7.Text = "7";
            this.poga7.UseVisualStyleBackColor = true;
            this.poga7.Click += new System.EventHandler(this.poga0_Click);
            // 
            // artcalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.vienads);
            this.Controls.Add(this.minuss);
            this.Controls.Add(this.pogareiz);
            this.Controls.Add(this.pogadal);
            this.Controls.Add(this.pogaplus);
            this.Controls.Add(this.poga0);
            this.Controls.Add(this.poga3);
            this.Controls.Add(this.poga9);
            this.Controls.Add(this.poga6);
            this.Controls.Add(this.poga2);
            this.Controls.Add(this.poga5);
            this.Controls.Add(this.poga8);
            this.Controls.Add(this.poga1);
            this.Controls.Add(this.poga4);
            this.Controls.Add(this.poga7);
            this.Name = "artcalc";
            this.Text = "artcalc";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button vienads;
        private System.Windows.Forms.Button minuss;
        private System.Windows.Forms.Button pogareiz;
        private System.Windows.Forms.Button pogadal;
        private System.Windows.Forms.Button pogaplus;
        private System.Windows.Forms.Button poga0;
        private System.Windows.Forms.Button poga3;
        private System.Windows.Forms.Button poga9;
        private System.Windows.Forms.Button poga6;
        private System.Windows.Forms.Button poga2;
        private System.Windows.Forms.Button poga5;
        private System.Windows.Forms.Button poga8;
        private System.Windows.Forms.Button poga1;
        private System.Windows.Forms.Button poga4;
        private System.Windows.Forms.Button poga7;
    }
}