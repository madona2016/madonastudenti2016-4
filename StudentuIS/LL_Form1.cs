﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LL_Kalkulators
{
    public partial class FormPamats : Form
    {
        double skaitlis = 0;
        string operators = "";
        bool oper_ievadits = false;

        public FormPamats()
        {
            InitializeComponent();
        }

        private void keyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9')
            {
                if (textBoxRezultats.Text == "0" || oper_ievadits)
                {
                    textBoxRezultats.Clear();
                    oper_ievadits = false;
                }
                textBoxRezultats.Text = textBoxRezultats.Text + e.KeyChar.ToString();
            }
        }

        private void cipars_click(object sender, EventArgs e)
        {
            if (textBoxRezultats.Text == "0" || oper_ievadits)
            {
                textBoxRezultats.Clear();
                oper_ievadits = false;
            }

            Button poga = (Button)sender;
            textBoxRezultats.Text = textBoxRezultats.Text + poga.Text;
        }

        private void buttonKomats_Click(object sender, EventArgs e)
        {
            if (!textBoxRezultats.Text.Contains(","))
                textBoxRezultats.Text = textBoxRezultats.Text + ",";
        }

        private void operators_click(object sender, EventArgs e)
        {
            Button poga = (Button)sender;
            oper_ievadits = true;

            if (skaitlis != 0)
                buttonVienads.PerformClick();
            else
                skaitlis = double.Parse(textBoxRezultats.Text);
            operators = poga.Text;
        }

        private void buttonVienads_Click(object sender, EventArgs e)
        {
            // Izņēmuma situācijas
            
            try
            {
                switch (operators)
                {
                    case "+":
                        textBoxRezultats.Text = (skaitlis + double.Parse(textBoxRezultats.Text)).ToString();
                        break;
                    case "-":
                        textBoxRezultats.Text = (skaitlis - double.Parse(textBoxRezultats.Text)).ToString();
                        break;
                    case "*":
                        textBoxRezultats.Text = (skaitlis * double.Parse(textBoxRezultats.Text)).ToString();
                        break;
                    case "/":
                        if (textBoxRezultats.Text == "0")   // Pārbaude dalīšanai ar nulli
                            throw new System.DivideByZeroException();
                        else
                            textBoxRezultats.Text = (skaitlis / double.Parse(textBoxRezultats.Text)).ToString();
                        break;
                    default:
                        break;
                } 
                skaitlis = double.Parse(textBoxRezultats.Text);
                oper_ievadits = true;
                operators = "";
            }

            //catch (DivideByZeroException ex)  // Nestrādā
            //{
            //    MessageBox.Show(ex.Message);
            //    buttonC.PerformClick();
            //}

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                buttonC.PerformClick();
            }

            //
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            textBoxRezultats.Text = "0";
            skaitlis = 0;
            operators = "";
        }

        private void buttonCE_Click(object sender, EventArgs e)
        {
            textBoxRezultats.Text = "0";
        }
    }
}
