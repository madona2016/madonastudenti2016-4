﻿namespace StudentuIS
{
    partial class JT_kalkulators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EditNumber_1 = new System.Windows.Forms.TextBox();
            this.EditMathOperator = new System.Windows.Forms.TextBox();
            this.EditNumber_2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.calculate = new System.Windows.Forms.Button();
            this.EditResult = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // EditNumber_1
            // 
            this.EditNumber_1.Location = new System.Drawing.Point(30, 25);
            this.EditNumber_1.Name = "EditNumber_1";
            this.EditNumber_1.Size = new System.Drawing.Size(57, 20);
            this.EditNumber_1.TabIndex = 0;
            // 
            // EditMathOperator
            // 
            this.EditMathOperator.Location = new System.Drawing.Point(109, 25);
            this.EditMathOperator.Name = "EditMathOperator";
            this.EditMathOperator.Size = new System.Drawing.Size(44, 20);
            this.EditMathOperator.TabIndex = 1;
            // 
            // EditNumber_2
            // 
            this.EditNumber_2.Location = new System.Drawing.Point(176, 25);
            this.EditNumber_2.Name = "EditNumber_2";
            this.EditNumber_2.Size = new System.Drawing.Size(57, 20);
            this.EditNumber_2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(124, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "=";
            // 
            // calculate
            // 
            this.calculate.Location = new System.Drawing.Point(30, 95);
            this.calculate.Name = "calculate";
            this.calculate.Size = new System.Drawing.Size(203, 23);
            this.calculate.TabIndex = 5;
            this.calculate.Text = "Rēķināt";
            this.calculate.UseVisualStyleBackColor = true;
            this.calculate.Click += new System.EventHandler(this.calculate_Click);
            // 
            // EditResult
            // 
            this.EditResult.Enabled = false;
            this.EditResult.Location = new System.Drawing.Point(81, 64);
            this.EditResult.Name = "EditResult";
            this.EditResult.Size = new System.Drawing.Size(100, 20);
            this.EditResult.TabIndex = 6;
            this.EditResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // JT_kalkulators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(257, 130);
            this.Controls.Add(this.EditResult);
            this.Controls.Add(this.calculate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.EditNumber_2);
            this.Controls.Add(this.EditMathOperator);
            this.Controls.Add(this.EditNumber_1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "JT_kalkulators";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "J.Tetarenko kalkulators";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox EditNumber_1;
        private System.Windows.Forms.TextBox EditMathOperator;
        private System.Windows.Forms.TextBox EditNumber_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button calculate;
        private System.Windows.Forms.TextBox EditResult;
    }
}